# ExerciciGitTerminal

Para empezar con el ejercicio he hecho un mkdir gitBasic dentro de mi carpeta de DADES, y he inicialicado el git con un git init, después con un ls -a se puede ver que hay un archivo oculto llamado .git.

Con un git status podemos ver el estado de nuestro repositorio.

Para crear un documento nuevo lo que he hecho ha sido hacer un nano videojoc.txt y he escrito dentro el texto. después con un git add videojoc.txt podemos ver que esta pendiente de commit en el repositorio, para confirmar que queremos subirlo haremos un git commit -m "inici document videojoc" y podemos comprobar que ya no esta pendiente de subida con git status.

Con un git log se pueden ver los commits hechos.

Para editar lo que hemos hecho solo con un nano en el fichero existente ya valdría y hacer lo mismo de antes, un git add videojoc.txt y un commit con mensaje.

También podemos crear un fichero oculto llamado .gitignore donde podremos poner los archivos que queremos que git ignore, como por ejemplo privat.txt que es el que pide el ejercicio.

Para añadir un branch se haría de esta forma: git branch game_system y para cambiar del branch actual al que hemos creado se haría así: git checkout game_system, con un git status se podría comprobar lo que hemos hecho, para hacer un merge de las branches se haría de esta forma: git merge game_system y finalmente para borrar el branch antiguo se haría así: git branch -d game_system
